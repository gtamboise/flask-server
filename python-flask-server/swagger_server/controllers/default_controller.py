import connexion
import six

from swagger_server import util


def resources_get():  # noqa: E501
    """resources_get

    Finds Resources # noqa: E501


    :rtype: List[str]
    """
    return 'do some magic!'


def resources_id_get(Id):  # noqa: E501
    """resources_id_get

    Returns a single Resource # noqa: E501

    :param Id: 
    :type Id: int

    :rtype: str
    """
    return 'do some magic!'


def resources_id_sub_resources_get(Id):  # noqa: E501
    """resources_id_sub_resources_get

    Returns sub resources # noqa: E501

    :param Id: 
    :type Id: int

    :rtype: str
    """
    return 'do some magic!'
