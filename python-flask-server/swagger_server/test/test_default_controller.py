# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_resources_get(self):
        """Test case for resources_get

        
        """
        response = self.client.open(
            '//resources',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_resources_id_get(self):
        """Test case for resources_id_get

        
        """
        response = self.client.open(
            '//resources/{Id}'.format(Id=56),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_resources_id_sub_resources_get(self):
        """Test case for resources_id_sub_resources_get

        
        """
        response = self.client.open(
            '//resources/{Id}/sub-resources'.format(Id=56),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
