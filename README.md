# Introduction

Quick playground for Swagger-generated API implemented in Flask, running in a container.
The API specifications are in the file [specifications.yaml](specifications.yaml).

# Getting started

Clone the git repo on a Docker-capable host.
Google Cloud Shell will do just fine.
```bash
git clone git@gitlab.com:gtamboise/flask-server.git
```

# Patch the swagger-editor generated requirements.txt

The version of `connexion` needs to be bumped up - look at the
[git changes](https://gitlab.com/gtamboise/flask-server/-/commit/df54c07d379d7207f1d58a94786c0af02d03c562).

# Build the Docker image

The Swagger editor assembles the server in the `python-flask-server` directory:

```bash
cd flask-server
docker build -t resources python-flask-server
```

# Start the container

```bash
docker run --rm -p 127.0.0.1:8080:8080 resources
```

# Test the container

For example from a new Cloud Shell tab (or a new window using `<CTRL>+B "`),
```bash
$ curl http://127.0.0.1:8080/resources
"do some magic!"
$ curl http://127.0.0.1:8080/resources/3141
"do some magic!"
$ curl http://127.0.0.1:8080/resources/1234/sub-resources
"do some magic!"
```

Notice how "do some magic" is the stubb defined in
[default_controller](python-flask-server/swagger_server/controllers/default_controller.py).

Notice how we do not have a `GET /` in the specifications,
so as expected asking for "/" returns a 404:
```bash
$ curl http://127.0.0.1:8080
{
  "detail": "The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.",
  "status": 404,
  "title": "Not Found",
  "type": "about:blank"
}
```

# Testing using the browser

For GET tests, a web browser running Google Cloud Shell will do just fine.

Click on `"Web Preview" > "Preview on port 8080"` (top right corner).
The initial request will `GET /`: as expected, it returns a 404.

Append `resources` to the URL, for example to get `https://8080-dot-4265959-dot-devshell.appspot.com/resources`,
and you will get "do some magic!".
